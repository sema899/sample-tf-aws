variable "prefix" {
    default = "sample"
}

variable "contact" {
    default = "ABC@xyz.com"
}