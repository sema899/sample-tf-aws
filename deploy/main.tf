terraform {
  backend "s3" {
    bucket         = "makes-gitlab-tf-state"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "makes_gitlab_tf_state_lock"
  }

}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix} - ${terraform.workspace}"
  common_tags {
    Environment = terraform.workspace
    owner = var.contact
    ManagedBy = "terraform"
    }
}

